// boolean

var ss1 = 1 == 1;
console.log("😀 - ss1", ss1);
var ss2 = 1 != 1;
console.log("😀 - ss2", ss2);
var ss3 = 2 > 1;
console.log("😀 - ss3", ss3);
var ss3 = 3 >= 3;
console.log("😀 - ss3", ss3);
var ss4 = 2 <= 2;
console.log("😀 - ss4", ss4);
// = gán
// == so sánh
var ss5 = 2 == "2"; //true
var ss6 = 2 === "2"; //false

// toán tử so sánh và &&, || ~ kết hợp nhiều phép so sánh lại với nhau

// so sánh && : chỉ đúng khi Tất Cả đều đúng

// var ss7 = 2 > 1 && 2 > 10;
var ss7 = true && false && true && true;
console.log("😀 - ss7", ss7);

var ss8 = false && false;

// so sánh hoặc ||: chỉ sai khi tất cả đều sai

var ss10 = true || false; //true

// &&
// ba người yêu: cháu có công việc, có tiền, không có nhà, siêng năng

// ||
// cháu làm IT, lương thấp, không có nhà,lười
